import tensorflow as tf
import os
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import classification_report
import pandas as pd

IMAGE_SIZE = 32


def load_data_from_file(file_path, labels_number=1):
    with open(file_path, 'rb') as f:
        data = f.read()
    offset = 0
    max_offset = len(data) - 1
    coarse_labels = []
    fine_labels = []
    images = []
    while offset < max_offset:
        labels = np.frombuffer(data, dtype=np.uint8, count=labels_number, offset=offset).reshape((labels_number,))
        offset += labels_number
        img = (np.frombuffer(data, dtype=np.uint8, count=3072, offset=offset).reshape(
            (3, IMAGE_SIZE, IMAGE_SIZE)).transpose((1, 2, 0)))
        offset += 3072
        coarse_labels.append(labels[0])
        fine_labels.append(labels[1])
        images.append(img)
    return [np.array(coarse_labels), np.array(fine_labels), np.array(images)]


def load_labels_from_file(file_path):
    return np.genfromtxt(file_path, comments="#", delimiter=",", dtype='str')


def load_cifar100_dataset():
    url = "https://www.cs.toronto.edu/~kriz/cifar-100-binary.tar.gz"
    dataset_file = tf.keras.utils.get_file("cifar.tar.gz", url, untar=True, cache_dir='.', cache_subdir='')
    dataset_dir = os.path.join(os.path.dirname(dataset_file), 'cifar-100-binary')

    CY_train, FY_train, X_train = load_data_from_file(os.path.join(dataset_dir, 'train.bin'), labels_number=2)
    CY_test, FY_test, X_test = load_data_from_file(os.path.join(dataset_dir, 'test.bin'), labels_number=2)

    C_label = np.genfromtxt(os.path.join(dataset_dir, 'coarse_label_names.txt'), comments="#", delimiter=",",
                            dtype='str')
    F_label = np.genfromtxt(os.path.join(dataset_dir, 'fine_label_names.txt'), comments="#", delimiter=",", dtype='str')

    return X_train, CY_train, FY_train, X_test, CY_test, FY_test, C_label, F_label


X_train, CY_train, FY_train, X_test, CY_test, FY_test, C_label, F_label = load_cifar100_dataset()

print(X_train.shape, CY_train.shape, FY_train.shape)

X_train, X_test = X_train / 255.0, X_test / 255.0


def view_dataset(classnames, label, images):
    plt.figure(figsize=(12, 12))
    for i in range(120):
        plt.subplot(10, 12, i + 1)
        plt.xticks([])
        plt.yticks([])
        plt.grid(False)
        plt.imshow(images[i])
        plt.xlabel(classnames[label[i]], labelpad=2, fontsize=6)
    plt.show()


# Вывод изображений с узкими классами
view_dataset(F_label, FY_train, X_train)

# Вывод изображений с широкими классами
view_dataset(C_label, CY_train, X_train)

# Построение модели

from tensorflow.keras import layers, models


def getModel():
    model = models.Sequential()
    model.add(layers.Conv2D(32, (3, 3), activation='relu', input_shape=(32, 32, 3)))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(64, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(128, (3, 3), activation='relu'))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Flatten())
    model.add(layers.Dense(256, activation='relu'))
    model.add(layers.Dense(128, activation='relu'))
    model.add(layers.Dense(100, activation='softmax'))
    return model


model = getModel()

model.summary()


# **Какие элементы сети зависят от количества цветов, какие — от количества классов?**

# Количество цветов влияет на конфигурацию входных слоев и размеры весовых тензоров,
# а количество классов влияет на конфигурацию выходного слоя, функцию активации и
# функцию потерь для правильной классификации изображений в задаче классификации.

# **Объяснение места в модели каждого слоя, обоснование выбора гиперпараметров.**

# 1. Входной слой представляет собой изображения размером 32x32 пикселя с тремя цветовыми каналами (RGB).
# Размер входных данных определяется размерами изображений и количеством цветовых каналов.
# В данном случае, изображения CIFAR-10 имеют 32x32x3 пикселя, что определяет размер входных данных

# 2. Сверточные слои идут после входного слоя.
# Обычно включают несколько сверточных слоев с пулингом.
# Сверточные слои выполняют операции свертки и пулинга для извлечения признаков из изображений.
# Гиперпараметры сверточных слоев, такие как количество фильтров, размеры фильтров, шаг свертки (stride) и тип пулинга,
# выбираются на основе экспериментов и могут зависеть от сложности задачи.

# 3. Полносвязанные слои следуют за сверточными слоями и представляют собой плоский вектор признаков для каждого
# изображения. Полносвязанные слои принимают высокоуровневые признаки из сверточных слоев и выполняют классификацию.
# Гиперпараметры полносвязных слоев, такие как количество нейронов и функции активации, выбираются на основе
# экспериментов.

# 4. Выходной слой находится в конце модели и отвечает за предсказание классов.
# Выходной слой должен иметь столько нейронов, сколько классов в задаче классификации (в случае CIFAR-10 - 10 классов).
# Функция активации на выходном слое обычно является softmax для многоклассовой классификации.

# 5. Функция потерь и оптимизатор: Эти элементы определяются при компиляции модели. Выбор функции потерь и
# оптимизатора зависит от задачи классификации и могут быть выбраны на основе принципов обучения нейронных сетей.
# Общие гиперпараметры, такие как количество сверточных слоев, размер фильтров, количество нейронов в полносвязных
# слоях и скорость обучения, выбираются на основе экспериментов и подбираются таким образом, чтобы обеспечить хорошую
# производительность модели на задаче классификации CIFAR-10.
def getCompiledModel():
    model = getModel()
    model.compile(loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  optimizer='adam',
                  metrics=['accuracy'])
    return model


# Обучение на 20 широких классах


model = getCompiledModel()

his = model.fit(X_train, CY_train, epochs=20)

# Экспорт обученной модели в файл

model.save('./coarse_model.keras')

# Точность на тестовых данных

С_test_loss, С_test_acc = model.evaluate(X_test, CY_test)

print('\nТочность на тестовых данных:', С_test_acc)

С_predictions = model.predict(X_test)

С_predictions = np.argmax(С_predictions, axis=-1)

print(classification_report(CY_test, С_predictions, target_names=C_label))

# Обучение на 100 узких классах


tf.keras.backend.clear_session()

model = model = getCompiledModel()

his = model.fit(X_train, FY_train, epochs=20)

# Экспорт обученной модели в файл

model.save('./fine_model.keras')

# Точность на тестовых данных

F_test_loss, F_test_acc = model.evaluate(X_test, FY_test)

print('\nТочность на тестовых данных:', F_test_acc)

F_predictions = model.predict(X_test)

F_predictions = np.argmax(F_predictions, axis=-1)

print(classification_report(FY_test, F_predictions, target_names=F_label))
F_report = classification_report(FY_test, F_predictions, target_names=F_label, output_dict=True)


# Сравнение


def init_fine_map():
    map = {
        "aquatic_mammals": ["beaver", "dolphin", "otter", "seal", "whale"],
        "fish": ["aquarium_fish", "flatfish", "ray", "shark", "trout"],
        "flowers": ["orchid", "poppy", "rose", "sunflower", "tulip"],
        "food_containers": ["bottle", "bowl", "can", "cup", "plate"],
        "fruit_and_vegetables": ["apple", "mushroom", "orange", "pear", "sweet_pepper"],
        "household_electrical_devices": ["clock", "keyboard", "lamp", "telephone", "television"],
        "household_furniture": ["bed", "chair", "couch", "table", "wardrobe"],
        "insects": ["bee", "beetle", "butterfly", "caterpillar", "cockroach"],
        "large_carnivores": ["bear", "leopard", "lion", "tiger", "wolf"],
        "large_man-made_outdoor_things": ["bridge", "castle", "house", "road", "skyscraper"],
        "large_natural_outdoor_scenes": ["cloud", "forest", "mountain", "plain", "sea"],
        "large_omnivores_and_herbivores": ["camel", "cattle", "chimpanzee", "elephant", "kangaroo"],
        "medium_mammals": ["fox", "porcupine", "possum", "raccoon", "skunk"],
        "non-insect_invertebrates": ["crab", "lobster", "snail", "spider", "worm"],
        "people": ["baby", "boy", "girl", "man", "woman"],
        "reptiles": ["crocodile", "dinosaur", "lizard", "snake", "turtle"],
        "small_mammals": ["hamster", "mouse", "rabbit", "shrew", "squirrel"],
        "trees": ["maple_tree", "oak_tree", "palm_tree", "pine_tree", "willow_tree"],
        "vehicles_1": ["bicycle", "bus", "motorcycle", "pickup_truck", "train"],
        "vehicles_2": ["lawn_mower", "rocket", "streetcar", "tank", "tractor"],
    }
    dict = {}
    for label in F_label:
        for key in map:
            if label in map[key]:
                dict.update({label: key})
                break
    return dict


fine_to_coarse = init_fine_map()

FC = lambda i: fine_to_coarse[F_label[i]]
CF_predictions = np.array([FC(i) for i in F_predictions])
C_index = lambda i: np.where(C_label == i)[0][0]
CF_predictions = np.array([C_index(i) for i in CF_predictions])

# Оценка обобщенных предсказаний по узким меткам до метки их широкого класса

print(classification_report(CY_test, CF_predictions, target_names=C_label))
CF_report = classification_report(CY_test, CF_predictions, target_names=C_label, output_dict=True)

# Оценка предсказаний при обучении на широких метках

print(classification_report(CY_test, С_predictions, target_names=C_label))
C_report = classification_report(CY_test, С_predictions, target_names=C_label, output_dict=True)

# В первом случае точность предсказания 0.43, во втором случае 0.45, что примерно одинаково, поэтому сложно точно
# сказать, какой из методов дает лучший результат.


# **Исследование с помощью графиков метрики предсказания для
# каких узких классов более всего отличаются от метрик их более широких классов**

F_precision = lambda i: F_report[i]['precision']
F_precisions = np.array([F_precision(i) for i in F_label])

CF_precision = lambda i: CF_report[fine_to_coarse[i]]['precision']
CF_precisions = np.array([CF_precision(i) for i in F_label])

C_precision = lambda i: C_report[fine_to_coarse[i]]['precision']
C_precisions = np.array([C_precision(i) for i in F_label])

df = pd.DataFrame(
    {
        'label': F_label,
        'c_label': np.array([fine_to_coarse[i] for i in F_label]),
        'F_CF': F_precisions - CF_precisions,
        'F_C': F_precisions - C_precisions,
        'CF_C': CF_precisions - C_precisions,
    })

# График различия точности распознавания узкого класса и точности распознавания широкого класса по узкому
df.sort_values(by=['c_label', 'F_CF'], ascending=True).plot.barh(y='F_CF', x='label', legend=False, figsize=(6, 16),
                                                                 ylabel='')

# График отображает разницу в точности классификации между узкими и широкими классами в нейронной сети. Он позволяет
# оценить, насколько нейронная сеть успешно распознает конкретные подклассы внутри более общих классов. Наибольший
# разброс имеют элементы широкого класса деревьев (trees) и людей (people), это говорит о том, что конкретный вид
# деревьев и людей между собой данная сеть определяет плохо. С другой стороны, наименьшее отклонение в точности
# классификации наблюдается для узких классов, таких как lawn_mover, turtle, worm, plain, road, keyboard, telephone,
# plate, bottle, такие сеть определяет хорошо. Иными словами, сеть более надежно и точно классифицирует узкие классы,
# в то время как у нее возникают трудности в распознавании более широких классов, где есть больше вариаций и
# разнообразия между конкретными видами объектов.


# График различия точности распознавания узкого класса и точности распознавания широкого класса
df.sort_values(by=['c_label', 'F_C'], ascending=True).plot.barh(y='F_C', x='label', legend=False, figsize=(6, 16),
                                                                ylabel='')

# График исследует различия в точности классификации между узкими и широкими классами в модели и позволяет сделать
# вывод о том, насколько успешно модель распознает более широкие классы по сравнению с узкими классами. Например,
# изображение сосны (pine_tree) в случае определения по широкому классу определяется лучше, чем по узкому. А
# изображения динозавра (dinosaur), дикообраза (porcupine), облака (cloud), дороги (road) определяются с примерно
# одинаковой точностью, как в случае распознавания по узкому классу, так и по широкому. Таким образом, график дает
# нам информацию о том, как модель проявляет свои способности в распознавании разных классов в зависимости от их
# ширины и сложности.


# График различия точности распознавания широкого класса по узкому и точности распознавания широкого класса
df.sort_values(by=['c_label', 'CF_C'], ascending=True).plot.barh(y='CF_C', x='label', legend=False, figsize=(6, 16),
                                                                 ylabel='')

# График, который сравнивает разницу в точности распознавания широких классов по сравнению с их распознаванием в
# рамках узких классов, позволяет сделать вывод о том, какие категории лучше распознаются при обучении на узких
# классах и какие при обучении на широких классах. На узких классах наблюдается лучшее качество распознавания (что
# выражается положительным значением на графике) для классов, таких как транспортные средства (vehicles_2), деревья,
# рептилии, беспозвоночные (кроме насекомых), природные сцены, контейнеры для пищи, водные млекопитающие и другие. В
# то время как другие классы показывают более высокую точность распознавания при обучении на широких классах. Это
# говорит о том, что для некоторых категорий объектов, обучение на более узких классах (более специфичных) может
# привести к более точной классификации, в то время как для других категорий объектов, обучение на более широких
# классах (более общих) может быть более эффективным.
